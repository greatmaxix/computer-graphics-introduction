let canvas = document.getElementById("myCanvas");
canvas.mozImageSmoothingEnabled = false;
canvas.webkitImageSmoothingEnabled = false;
canvas.msImageSmoothingEnabled = false;
canvas.imageSmoothingEnabled = false;
canvas.mozImageSmoothingEnabled = false;
let ctx = canvas.getContext("2d");
let slider = document.getElementById("slider");
let degreeLabel ;
let degree = 0;
let radian = 0;
let animationStarted = false;
let normal = false;
let precisionInput = 10000;
let figureType = 1;
let shade = false;

window.onload = () => {
    drawDonut();
    degreeLabel = document.getElementById("degree");
    degreeLabel.innerText = degree.toString();
};

function ShadingOn(event) {
    shade = !shade;
    drawDonut();
}

function changeFigure(event) {
    if (event.value == 1) {
        //torus
        figureType = 1;
    }
    else if (event.value == 2) {
        //sphere
        figureType = 2;
    }
    drawDonut();
}

let smallerRadius = 100;
let biggerRadius = 200;


function rotateByX(rotatedPoint, Radian) {
    rotatedPoint.x = rotatedPoint.x;
    rotatedPoint.y = rotatedPoint.y * Math.cos(Radian) - rotatedPoint.z * Math.sin(Radian);
    rotatedPoint.z = rotatedPoint.y * Math.sin(Radian) + rotatedPoint.z * Math.cos(Radian);
    return rotatedPoint;
}

function animatePara(event) {
    let interval = setInterval(() => {
        if (degree <= 0 || degree > 360)
            degree = 1;

        degree += 1;
        drawDonut()
    }, 10);
}

function Normals(event) {
    normal = !normal;
    drawDonut();
    //animatePara();
}

function accuracysliderclick(event) {
    precisionInput = event.value;
    drawDonut();
}

function drawDonut() {
    cleanCanvas();
    let cameraPos = {x:0, y:0, z: -500};
    //how to relativly change camera pos?
    //cameraPos = rotateByX(cameraPos, radian);
    //console.log("updated", cameraPos);
    //drawPoint(ctx, {x: cameraPos.x + 250, y: cameraPos.y + 250, z: cameraPos.z + 250}, 'red', 'blue', 1);
    let torusBmodel = figureType == 1 ? getTorusModel() : getSphereModel();
    let prevEdgeList = [];
    counter = 0;
    for (let i = 0; i < torusBmodel.rectangles.length; i++) {
        let currRectangle = torusBmodel.rectangles[i];
        /*
        let crossProd = getCrossProductOfVectors(currRectangle.p4, currRectangle.p1);
        console.log(crossProd);
        drawLine(currRectangle.p3, crossProd);
        let dotProdOfCameraAndNormal = dotProduct(cameraPos, crossProd);
        //if (dotProdOfCameraAndNormal >= 0) {
            drawRectangle(currRectangle);
            fillRectangle(ctx, currRectangle);
        //}*/

         //uses triangles!
        let vector1A = substractVector(currRectangle.p2, currRectangle.p3); //currRectangle.p1;
        let vector1B = substractVector(currRectangle.p1, currRectangle.p3) //currRectangle.p2; 
        let crossProd1 = getCrossProductOfVectors(vector1A, vector1B); //surface normal

        let vector2A = substractVector(currRectangle.p2, currRectangle.p3); //currRectangle.p2;
        let vector2B = substractVector(currRectangle.p4, currRectangle.p3) //currRectangle.p4;
        let crossProd2 = getCrossProductOfVectors(vector2B, vector2A); //surface normal


        
        let dotProdOfCameraAndNormal1 = dotProduct(cameraPos, crossProd1);
        let dotProdOfCameraAndNormal2 = dotProduct(cameraPos, crossProd2);

        if (normal) {
            let centroid1 = {
                x: (currRectangle.p1.x + currRectangle.p2.x + currRectangle.p3.x) / 3,
                y: (currRectangle.p1.y + currRectangle.p2.y + currRectangle.p3.y) / 3,
                z: (currRectangle.p1.z + currRectangle.p2.z + currRectangle.p3.z) / 3,
            };
            strokeLine(ctx, centroid1, crossProd1);
            strokeLine(ctx, centroid1, crossProd2);
        }

        let currTriangle1 = {p1: currRectangle.p1, p2: currRectangle.p2, p3: currRectangle.p3};
        let currTriangle2 = {p1: currRectangle.p2, p2: currRectangle.p3, p3: currRectangle.p4};
        let rayVector// = substractVector({x:400, y: 400, z: 400}, {x:10, y: 10, z: 10});
        let facingRatio// = Math.round(dotProduct(crossProd1, rayVector) / 100000);
        let fillColor = "rgba(250, 0, 0, 1)";

        if (dotProdOfCameraAndNormal1 >= 0 && shade) {
            //shading
            rayVector = substractVector({x:400, y: 400, z: 400}, {x:10, y: 10, z: 10});
            facingRatio = Math.round(dotProduct(crossProd1, rayVector) / 100000);
            if (facingRatio == 1) {
                fillColor = "rgba(250, 0, 0, 1)";
            }
            else if (0 < facingRatio && facingRatio < 1) {
                fillColor = "rgba(184, 0, 0, 1)";
            }
            else if (facingRatio == 0) {
                fillColor = "rgba(117, 0, 0, 1)";
            }
            else if (facingRatio < 0) {
                fillColor = "rgba(46, 0, 0, 1)";
            }
            drawTriangle(currTriangle1);
            fillTriangle(ctx, currTriangle1, fillColor);
        }
        else if (!shade) {
            drawTriangle(currTriangle1);
            counter+=1;
        }
        if (dotProdOfCameraAndNormal2 >= 0 && shade) {

            rayVector = substractVector({x:400, y: 400, z: 400}, {x:10, y: 10, z: 10});
            facingRatio = Math.round(dotProduct(crossProd2, rayVector) / 100000);
            //fix 
            if (facingRatio == 1) {
                fillColor = "rgba(250, 0, 0, 1)";
            }
            else if (0 < facingRatio && facingRatio < 1) {
                fillColor = "rgba(184, 0, 0, 1)";
            }
            else if (facingRatio == 0) {
                fillColor = "rgba(117, 0, 0, 1)";
            }
            else if (facingRatio < 0) {
                fillColor = "rgba(46, 0, 0, 1)";
            }
            drawTriangle(currTriangle2);
            fillTriangle(ctx,currTriangle2, fillColor);
        }
        else if(!shade){
            drawTriangle(currTriangle2);
            counter+=1;
        }
        
    }
    console.log("counter of not drawn triangles:", counter);
}

function fillTriangle(ctx,currTriangle, fillStyle = 'rgba(255,165,1,99)') {
    ctx.fillStyle = fillStyle;
    ctx.moveTo(currTriangle.p1.x, currTriangle.p1.y);
    ctx.lineTo(currTriangle.p2.x, currTriangle.p2.y); 
    ctx.lineTo(currTriangle.p3.x, currTriangle.p3.y); 
    ctx.fill(); // connect and fill
}

function substractVector(vector, origin) {
    return {
        x: vector.x - origin.x, 
        y: vector.y - origin.y,
        z: vector.z - origin.z,
    }
}

function getCrossProductOfVectors(v, w) {
    let result = {
        x: v.y * w.z  - v.z * w.y,
        y: v.z * w.x - v.x * w.z,
        z: v.x * w.y - v.y * w.x
    }
    return  result;
}

function dotProduct(v, w) {
    let dotprod = v.x * w.x + v.y * w.y + v.z * w.z;
    return dotprod;
}

function drawRectangle(rectangle) {
    strokeLine(ctx, rectangle.p1, rectangle.p2);
    strokeLine(ctx, rectangle.p2, rectangle.p4);
    strokeLine(ctx, rectangle.p3, rectangle.p4);
    strokeLine(ctx, rectangle.p3, rectangle.p1);
}

function drawTriangle(triangle) {
    strokeLine(ctx, triangle.p1, triangle.p2);
    strokeLine(ctx, triangle.p2, triangle.p3);
    strokeLine(ctx, triangle.p3, triangle.p1);
}

function fillRectangle(ctx, rectangle) {

}


function linesBetweenSmallCircles(p1, p2) {
    for (let i = 0; i < p1.length; i++) {
        strokeLine(ctx, p1[i].to, p2[i].to);
    }
}

function strokeLine(ctx, p1, p2) {
    ctx.beginPath();
    ctx.strokeStyle = 'black';
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.stroke();
}

function getTorusModel() {
    let vertices = [];
    let edges = [];
    let rectangles = [];
    let translateX = canvas.width / 2;
    let translateY = canvas.height / 2;
    let transform = 10;

    let i = 0;
    let smallerPrecision = 1;
    let s = smallerPrecision;
    
    let precision = precisionInput / 10000; //0.01
    
    
    //bigger circle
    for (i = 0; i < (Math.PI * 2); i += precision) {
        let pointToDraw = getTorusAtT(i, s, transform);
        let rotatedPoint = rotateByX(pointToDraw, radian);
        rotatedPoint.x = rotatedPoint.x + translateX;
        rotatedPoint.y = rotatedPoint.y + translateY;
        rotatedPoint.z = rotatedPoint.z + translateY;
        let previousPoint = rotatedPoint;
        //smaller circles
        let firstPoint = rotatedPoint;
        let currEdges = [];
        let currVertices = [];
        for (s = 0; s < (Math.PI * 2); s+= smallerPrecision) {
            pointToDraw = getTorusAtT(i, s, transform);
            rotatedPoint = rotateByX(pointToDraw, radian);
            
            rotatedPoint.x = rotatedPoint.x + translateX;
            rotatedPoint.y = rotatedPoint.y + translateY;
            rotatedPoint.z = rotatedPoint.z + translateY;

            currVertices.push(rotatedPoint);
            currEdges.push({from: previousPoint, to: rotatedPoint});
            previousPoint = rotatedPoint;
        }
        currEdges.push({from: rotatedPoint, to: firstPoint});
        firstPoint = rotatedPoint;
        edges.push(currEdges);
        vertices.push(currVertices);
    }
    rectangles = getRectanglesFromVertices(vertices);
    return { edges: edges, vertices: vertices, rectangles: rectangles };
}

function getSphereModel() {
    let vertices = [];
    let edges = [];
    let rectangles = [];
    let translateX = canvas.width / 2;
    let translateY = canvas.height / 2;
    let transform = 10;

    let i = 0;
    let smallerPrecision = 0.1;
    let s = 0;
    
    let precision = precisionInput / 10000; //0.01
    console.log(precision);
    
    //bigger circle
    for (i = 0; i < (Math.PI * 2); i += precision) {
        let pointToDraw = getSphereAtT(i, s);
        let rotatedPoint = rotateByX(pointToDraw, radian);
        rotatedPoint.x = rotatedPoint.x + translateX;
        rotatedPoint.y = rotatedPoint.y + translateY;
        rotatedPoint.z = rotatedPoint.z + translateY;
        let previousPoint = rotatedPoint;
        //smaller circles
        let firstPoint = rotatedPoint;
        let currEdges = [];
        let currVertices = [];
        for (s = 0; s < Math.PI; s+= smallerPrecision) {
            pointToDraw = getSphereAtT(i, s);
            rotatedPoint = rotateByX(pointToDraw, radian);
            
            rotatedPoint.x = rotatedPoint.x + translateX;
            rotatedPoint.y = rotatedPoint.y + translateY;
            rotatedPoint.z = rotatedPoint.z + translateY;

            currVertices.push(rotatedPoint);
            currEdges.push({from: previousPoint, to: rotatedPoint});
            previousPoint = rotatedPoint;
        }
        currEdges.push({from: rotatedPoint, to: firstPoint});
        firstPoint = rotatedPoint;
        edges.push(currEdges);
        vertices.push(currVertices);
    }
    rectangles = getRectanglesFromVertices(vertices);
    return { edges: edges, vertices: vertices, rectangles: rectangles };
}

function getRectanglesFromVertices(vertices) {
    let rectangleList = [];
    for (let i = 0; i < vertices.length; i++) {
        let currVericeList = vertices[i];
        
        let nextVertice;
        if (vertices[i + 1]) {
            nextVertice = vertices[i+1];
            let currRects = getRectangles(currVericeList, nextVertice);
            rectangleList = [...rectangleList, ...currRects];
        }
    }
    let lastOnes = getRectangles(vertices[vertices.length - 1], vertices[0]);
    rectangleList = [...rectangleList, ...lastOnes];
    return rectangleList;
}

function getRectangles(firstVerticeList, secondVerticeList) {
    
    let currVertices = JSON.parse(JSON.stringify(firstVerticeList));
    let nextVertices = JSON.parse(JSON.stringify(secondVerticeList));
    currVertices.push(currVertices[0]);
    nextVertices.push(nextVertices[0]);
    let rectangleList = [];

    for (let i = 0; i < currVertices.length - 1; i++) {
        let rectangle = { p1: 0, p2: 0, p3: 0, p4: 0 };
        rectangle.p1 = currVertices[i];
        rectangle.p2 = currVertices[i + 1];
        rectangle.p3 = nextVertices[i];
        rectangle.p4 = nextVertices[i + 1];
        rectangleList.push(rectangle);
    }
    return rectangleList;
}

function drawXYZVectors() {
    ctx.beginPath();
    //x
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.fillStyle = 'red';
    ctx.fill();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'red';
    //y
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width / 2, canvas.height);
    ctx.fillStyle = 'red';
    ctx.fill();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'red';

    ctx.stroke();
    ctx.closePath();
}

function multiplyVector(vector1, vector2) {
    return {
        x: vector1.x * vector2.x, 
        y: vector1.y * vector2.y,
        z: vector1.z * vector2.z
    }
}

function divideVector(vector1, vector2) {
    return {
        x: vector1.x / vector2.x, 
        y: vector1.y / vector2.y,
        z: vector1.z / vector2.z
    }
}

function sliderclick(event) {
    degree = document.getElementById("slider").value; 
    degreeLabel.innerText = degree.toString();
    radian = degree * Math.PI / 180;
    drawDonut();
}

function cleanCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function getTorusAtT(t, s, transform) {
    let point3D = { x: 0, y: 0, z: 0 };
    let innerRadius = 20;
    let tubeRadius = 10;
    //point3D.x = (3 * Math.cos(t) + tubeRadius * Math.cos(t) * (Math.cos(s) + innerRadius)) // * transform;
    //point3D.y = (3 * Math.sin(t) + tubeRadius * Math.sin(t) * (Math.cos(s) + innerRadius))// * transform;
    //point3D.z =  tubeRadius * Math.sin(s)// * transform;
    point3D.x = (3 * Math.sin(t) + tubeRadius * Math.sin(t) * (Math.cos(s) + innerRadius))// * transform;
    point3D.y = (3 * Math.cos(t) + tubeRadius * Math.cos(t) * (Math.cos(s) + innerRadius)) // * transform;
    point3D.z =  tubeRadius * Math.sin(s)// * transform;
    return point3D;
}

function getSphereAtT(t, s) {
    let point3D = { x: 0, y: 0, z: 0 };
    let r = 100;
    point3D.x = r * Math.cos(t) * Math.sin(s);
    point3D.y = r * Math.sin(t) * Math.sin(s);
    point3D.z = r * Math.cos(t);
    return point3D;
}

function getNormalVector(v, w) {
    let normVect = {
        x: v.y * w.z  - v.z * w.y,
        y: v.z * w.x - v.x * w.z,
        z: v.x * w.y - v.y * w.x
    };
    return normVect;
}

function drawPoint(ctx, point, color = 'black', fill = 'pink', lineWidth = 1) {
    //if (point.x && point.y) {
        let pointToBeDrawn = point;
        ctx.beginPath();
        ctx.arc(pointToBeDrawn.x, pointToBeDrawn.y, 2, 0, 2 * Math.PI, false);
        ctx.fillStyle = fill;
        ctx.fill();
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = color;
        ctx.stroke();
        ctx.closePath();
    //}
}

function drawLine(point1, point2, translateX = 0, translateY = 0) {
    ctx.beginPath();
    ctx.moveTo(point1.x + translateX, point1.y + translateY);
    ctx.lineTo(point2.x + translateX, point2.y + translateY);
    ctx.stroke();
    ctx.closePath();
}