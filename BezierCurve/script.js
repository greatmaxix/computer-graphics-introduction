let canvas = document.getElementById("myCanvas");
canvas.mozImageSmoothingEnabled = false;
canvas.webkitImageSmoothingEnabled = false;
canvas.msImageSmoothingEnabled = false;
canvas.imageSmoothingEnabled = false;
canvas.mozImageSmoothingEnabled = false;
canvas.addEventListener('mousedown', mouseDownEventHandler);
canvas.addEventListener('mouseup', mouseUpEventHandler);

let ctx = canvas.getContext("2d");
let points1 = [];
let pointRadius = 6;
let gotcha = false;
let foundPointIndex;
let slider = document.getElementById("slider");
let sliderValue = 100;

function sliderclick(event) {
    sliderValue = document.getElementById("slider").value;
    cleanCanvas();
    for (let i = 0; i < points1.length; i++) {
        drawPoint(points1[i]);
    }w
    drawBezier(points1);
}

function animateB(event) {
    sliderValue = 0;
    let interval = setInterval(function() { 
        sliderValue += 1;
        cleanCanvas();
        points1.forEach((point) => {
            drawPoint(point);
        })
        drawBezier(points1); 
        if (sliderValue == 100) {
            clearInterval(interval);
        }
    }, 
    10);
}

function cleanCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function goOn(event) {
    if (points1.length == 4) {
        let newpoints = getOpositePoints(points1);
        drawBezier(newpoints);
    }
}

function getOpositePoints(points) {
    if (points.length == 4) {
        let newpoints = [];
        newpoints[0] = points[3];
        let secDiff = { x: (points[0].x - points[1].x), y: (points[0].y - points[1].y) }
        newpoints[1] = { x: newpoints[0].x + secDiff.x, y: newpoints[0].y + secDiff.y };

        let thirdiff = { x: (points[1].x - points[2].x), y: (points[1].y - points[2].y) }
        newpoints[2] = { x: newpoints[1].x - thirdiff.x, y: newpoints[1].y + thirdiff.y };

        let forthdiff = { x: (points[2].x - points[3].x), y: (points[2].y - points[3].y) }
        newpoints[3] = { x: newpoints[2].x + forthdiff.x, y: newpoints[2].y + forthdiff.y };
        points1 = newpoints;

        return newpoints;
    }
}

function cleanPoints() {
    points1 = [];
    gotcha = false;
    cleanCanvas();
    foundPointIndex = -1;
}

function addPoint(clickCoords) {
    if (points1.length != 4)
        points1.push(clickCoords);
}

function mouseDownEventHandler(event) {
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    let clickedCoords = { x: x, y: y };
    if (points1.length == 4) {
        let foundPoint = points1.findIndex((currPoint) =>
            ((clickedCoords.x >= currPoint.x - (pointRadius / 2) && clickedCoords.x <= currPoint.x + (pointRadius / 2))
                && (clickedCoords.y >= currPoint.y - (pointRadius / 2) && clickedCoords.y <= currPoint.y + (pointRadius /2))) 
        );

        if (foundPoint != -1) {
            foundPointIndex = foundPoint;
            gotcha = true;
        }
    }
    else {
        addPoint(clickedCoords);
    }
}

function mouseUpEventHandler(event) {
    if (gotcha && foundPointIndex != -1) {
        let rect = canvas.getBoundingClientRect();
        let x = event.clientX - rect.left;
        let y = event.clientY - rect.top;
        clickCoords = { x: x, y: y };
        cleanCanvas();
        points1[foundPointIndex] = clickCoords;
        points1.forEach((point, index) => {
            drawPoint(point);
        })
        foundPointIndex = -1;
        gotcha = false;
        drawBezier(points1);
    }

    if (points1.length == 4) {
        drawPoint(points1[points1.length -1]);
        drawBezier(points1);
    }
    else 
        drawPoint(points1[points1.length -1]);
}

function bezier(t, p0, p1, p2, p3) {
    let cX = 3 * (p1.x - p0.x),
        bX = 3 * (p2.x - p1.x) - cX,
        aX = p3.x - p0.x - cX - bX;
    let cY = 3 * (p1.y - p0.y),
        bY = 3 * (p2.y - p1.y) - cY,
        aY = p3.y - p0.y - cY - bY;
    let x = (cX * t) + p0.x + (bX * Math.pow(t,2)) + (aX * Math.pow(t, 3));
    let y = (cY * t) + p0.y + (bY * Math.pow(t,2)) + (aY * Math.pow(t, 3));

    return { x: x, y: y }
}

function getTangentPoint(t, p0, p1, p2, p3) {
    let cX = 3 * (p1.x - p0.x),
        bX = 3 * (p2.x - p1.x) - cX,
        aX = p3.x - p0.x - cX - bX;
    let cY = 3 * (p1.y - p0.y),
        bY = 3 * (p2.y - p1.y) - cY,
        aY = p3.y - p0.y - cY - bY;
    let x = (cX) + (2 * bX * t) + (3 * aX * Math.pow(t, 2));
    let y = (cY) + (2 * bY * t) + (3 * aY * Math.pow(t, 2));   
    return { x: x, y: y };
}

function drawBezier(points) {
    for (let i = 0; i < points.length; i++) {
        if (points[i + 1]) {
            drawLinesBetweenPoints(points[i], points[i +1]);
        }
    }
    let maxValue = sliderValue / 100;
    let accuracy = 0.01;
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'blue';
    let p0 = points[0],
        p1 = points[1],
        p2 = points[2],
        p3 = points[3];

    console.log(points);

    let q0, q1, q2, r1, r2;

    q0 = getQ(0, maxValue, p0, p1, p2, p3);
    q1 = getQ(1, maxValue, p0, p1, p2, p3);
    q2 = getQ(2, maxValue, p0, p1, p2, p3);
    drawPointLine(q0, q1);
    drawPointLine(q1, q2);
    ctx.moveTo(p0.x, p0.y);

    for (let t=0; t<maxValue; t+=accuracy) {
        let b = bezier(t, p0, p1, p2, p3);
        //let bTangent = getTangentPoint(t, p0, p1, p2, p3);
        ctx.lineTo(b.x, b.y);
        //ctx.lineTo(bTangent.x, bTangent.y);
    }
    ctx.stroke();
}

function getQ(index, t, p0, p1, p2, p3) {
    let q = { x: 0, y: 0};
    if (index == 0) {;
        q.x = (1 - t) * p0.x + t * p1.x;
        q.y = (1 - t) * p0.y + t * p1.y;
    }
    else if (index == 1) {
        q.x = (1 - t) * p1.x + t * p2.x;
        q.y = (1 - t) * p1.y + t * p2.y;
    }
    else if (index == 2) {
        q.x = (1 - t) * p2.x + t * p3.x;
        q.y = (1 - t) * p2.y + t * p3.y;
    }
    return q;
}

function drawLinesBetweenPoints(a, b) {
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'red';
    ctx.moveTo(a.x, a.y);
    ctx.lineTo(b.x, b.y);    
    ctx.stroke();
}

function drawPointLine(a, b) {
    drawPoint(a);
    drawPoint(b);
    drawLinesBetweenPoints(a, b);
}

function multiplyPoint(v1, v2) {
    return { x: v1.x * v2.x, y: v2.y * v2.y }
}

function dividePoint(v1, v2) {
    return { x: v1.x / v2.x, y: v2.y / v2.y }
}

function drawPoint(x) {
    if (x) {
        let pointToBeDrawn = x;
        ctx.beginPath();
        ctx.arc(pointToBeDrawn.x, pointToBeDrawn.y, pointRadius, 0, 2 * Math.PI, false);
        ctx.fillStyle = 'pink';
        ctx.fill();
        ctx.lineWidth = 2;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        ctx.closePath();
    }
}

