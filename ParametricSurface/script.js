let canvas = document.getElementById("myCanvas");
canvas.mozImageSmoothingEnabled = false;
canvas.webkitImageSmoothingEnabled = false;
canvas.msImageSmoothingEnabled = false;
canvas.imageSmoothingEnabled = false;
canvas.mozImageSmoothingEnabled = false;
let ctx = canvas.getContext("2d");
let slider = document.getElementById("slider");
let degreeLabel ;
let degree = 0;
let radian = 0;
let animationStarted = false;
let backface = false;
let precisionInput = 1000;

window.onload = () => {
    drawDonut();
    degreeLabel = document.getElementById("degree");
    degreeLabel.innerText = degree.toString();
};

let smallerRadius = 100;
let biggerRadius = 200;


function rotateByX(rotatedPoint, Radian) {
    rotatedPoint.x = rotatedPoint.x;
    rotatedPoint.y = rotatedPoint.y * Math.cos(Radian) - rotatedPoint.z * Math.sin(Radian);
    rotatedPoint.z = rotatedPoint.y * Math.sin(Radian) + rotatedPoint.z * Math.cos(Radian);
    return rotatedPoint;
}

function animatePara(event) {
    animationStarted = !animationStarted;
    event.innerText = "Stop animation";
    let interval = setInterval(function() { 
        if (degree == 360) {
            degree = 0;
            radian = degree * Math.PI / 180;
        }
        degree += 1;
        drawDonut();
        if (!animationStarted) {
            clearInterval(interval);
            event.innerText = "Animate";
        }
    }, 
    500);
}

function backfaceCulling(event) {
    backface = true;
    drawDonut();
}

function accuracysliderclick(event) {
    precisionInput = event.value;
    drawDonut();
}

function drawDonut() {
    cleanCanvas();
    let translateX = canvas.width / 2;
    let translateY = canvas.height / 2;
    let transform = 10;

    let i = 0;
    let s = Math.PI;
    let pointToDraw = getTorusAtT(i, s, transform);
    
    let rotatedPoint = rotateByX(pointToDraw, radian);
    ctx.beginPath();
    ctx.moveTo(rotatedPoint.x + translateX, rotatedPoint.y + translateY);
    let precision = precisionInput / 10000; //0.01
    let previousPoint = rotatedPoint;
    for (i = precision; i < (Math.PI * 2); i += precision) {
        for (s = precision; s < (Math.PI * 2); s+= precision) {
            pointToDraw = getTorusAtT(i, s, transform);
            rotatedPoint = rotateByX(pointToDraw, radian);
            previousPoint = rotatedPoint;
            ctx.lineTo(rotatedPoint.x + translateX, rotatedPoint.y + translateY);    
        }
    }
    ctx.stroke();
    ctx.closePath();
}

function multiplyVector(vector1, vector2) {
    console.log("multiplyVector", vector1, vector2);
    return {
        x: vector1.x * vector2.x, 
        y: vector1.y * vector2.y,
        z: vector1.z * vector2.z
    }
}

function divideVector(vector1, vector2) {
    console.log("divideVector", vector1, vector2);
    return {
        x: vector1.x / vector2.x, 
        y: vector1.y / vector2.y,
        z: vector1.z / vector2.z
    }
}

function sliderclick(event) {
    degree = document.getElementById("slider").value; 
    degreeLabel.innerText = degree.toString();
    radian = degree * Math.PI / 180;
    drawDonut();
}

function cleanCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function getTorusAtT(t, s, transform) {
    let point3D = { x: 0, y: 0, z: 0 };
    let innerRadius = 20;
    let tubeRadius = 10;
    point3D.x = (3 * Math.cos(t) + tubeRadius * Math.cos(t) * (Math.cos(s) + innerRadius)) // * transform;
    point3D.y = (3 * Math.sin(t) + tubeRadius * Math.sin(t) * (Math.cos(s) + innerRadius))// * transform;
    point3D.z =  tubeRadius * Math.sin(s)// * transform;
    return point3D;
}

function getNormalVector(v, w) {
    return {
        x: v.y * w.z  - x.z * w.y,
        y: v.z * w.x - v.x * w.z,
        z: v.x * w.y - v.y * w.x
    }
}

function dotProduct(v, w) {
    return {

    }
}

function drawPoint(point, color = 'black', fill = 'pink') {
    if (point.x && point.y) {
        let pointToBeDrawn = point;
        ctx.beginPath();
        ctx.arc(pointToBeDrawn.x, pointToBeDrawn.y, 3, 0, 2 * Math.PI, false);
        ctx.fillStyle = fill;
        ctx.fill();
        ctx.lineWidth = 2;
        ctx.strokeStyle = color;
        ctx.stroke();
        ctx.closePath();
    }
}