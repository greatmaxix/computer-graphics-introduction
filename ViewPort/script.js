let canvas = document.getElementById("myCanvas");
canvas.mozImageSmoothingEnabled = false;
canvas.webkitImageSmoothingEnabled = false;
canvas.msImageSmoothingEnabled = false;
canvas.imageSmoothingEnabled = false;
canvas.mozImageSmoothingEnabled = false;

let ctx = canvas.getContext("2d");

let secondCanvas = document.getElementById("secondCanvas");
secondCanvas.mozImageSmoothingEnabled = false;
secondCanvas.webkitImageSmoothingEnabled = false;
secondCanvas.msImageSmoothingEnabled = false;
secondCanvas.imageSmoothingEnabled = false;
secondCanvas.mozImageSmoothingEnabled = false;


secondCanvas.addEventListener('mousedown', mouseDownEventHandler);
secondCanvas.addEventListener('mouseup', mouseUpEventHandler);

let rectPoints = [];

function windowChangeSize(event) {
    canvas.width = event.value;
    canvas.height = event.value;
    drawBezier();
    Transfer(event);
}

function mouseDownEventHandler(event) {
    let rect = secondCanvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    let clickedCoords = { x: x, y: y };
    if (rectPoints.length == 2) {
        console.log("rectPoints", rectPoints);
        console.log("clickedCoords", clickedCoords);
        let foundPoint = rectPoints.findIndex((currPoint) =>
            ((clickedCoords.x >= currPoint.x - (pointRadius / 2) && clickedCoords.x <= currPoint.x + (pointRadius / 2))
                && (clickedCoords.y >= currPoint.y - (pointRadius / 2) && clickedCoords.y <= currPoint.y + (pointRadius /2))) 
        );

        if (foundPoint != -1) {
            foundPointIndex = foundPoint;
            gotcha = true;
        }
    }
    else {
        addPoint(clickedCoords);
    }
}

function mouseUpEventHandler(event) {
    console.log("Gotcha", gotcha);
    console.log("foundPointIndex", foundPointIndex);
    if (gotcha && foundPointIndex != -1) {
        
        let rect = secondCanvas.getBoundingClientRect();
        let x = event.clientX - rect.left;
        let y = event.clientY - rect.top;
        clickCoords = { x: x, y: y };
        cleanCanvas(2);
        rectPoints[foundPointIndex] = clickCoords;
        foundPointIndex = -1;
        gotcha = false;
    }
    points.forEach((point, index) => {
        drawPoint(index);
    })

    if (rectPoints.length == 2) {
        Transfer(event);
    }
}

function addPoint(clickCoords) {
    if (rectPoints.length != 2)
        rectPoints.push(clickCoords);
}

let scndCtx = secondCanvas.getContext("2d");

let firstCoord;
let secondCoord;

let points = [{x: 117, y: 337.1875}, {x: 117, y: 98.1875}, {x: 312, y: 137.1875}, {x: 337, y: 349.1875}];
let pointRadius = 10;
let gotcha = false;
let foundPointIndex;


window.onload = () => {
    drawBezier();
}

//#region Bezier
function bezier(t, p0, p1, p2, p3) {
    let cX = 3 * (p1.x - p0.x),
        bX = 3 * (p2.x - p1.x) - cX,
        aX = p3.x - p0.x - cX - bX;
    let cY = 3 * (p1.y - p0.y),
        bY = 3 * (p2.y - p1.y) - cY,
        aY = p3.y - p0.y - cY - bY;
    let x = (cX * t) + p0.x + (bX * Math.pow(t,2)) + (aX * Math.pow(t, 3));
    let y = (cY * t) + p0.y + (bY * Math.pow(t,2)) + (aY * Math.pow(t, 3));

    return { x: x, y: y }
}

function getQ(index, t, p0, p1, p2, p3) {
    let q = { x: 0, y: 0};
    if (index == 0) {;
        q.x = (1 - t) * p0.x + t * p1.x;
        q.y = (1 - t) * p0.y + t * p1.y;
    }
    else if (index == 1) {
        q.x = (1 - t) * p1.x + t * p2.x;
        q.y = (1 - t) * p1.y + t * p2.y;
    }
    else if (index == 2) {
        q.x = (1 - t) * p2.x + t * p3.x;
        q.y = (1 - t) * p2.y + t * p3.y;
    }
    return q;
}

function drawBezier() {
    let maxValue = 1;
    let accuracy = 0.01;
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'blue';
    let p0 = points[0],
        p1 = points[1],
        p2 = points[2],
        p3 = points[3];

    let q0, q1, q2, r1, r2;

    q0 = getQ(0, maxValue, p0, p1, p2, p3);
    q1 = getQ(1, maxValue, p0, p1, p2, p3);
    q2 = getQ(2, maxValue, p0, p1, p2, p3);
    ctx.moveTo(p0.x, p0.y);

    for (let t=0; t<maxValue; t+=accuracy) {
        let b = bezier(t, p0, p1, p2, p3);
        ctx.lineTo(b.x, b.y);
    }
    ctx.stroke();
}

//#endregion

function windowtoViewport(x_w, y_w, x_wmax, y_wmax, x_wmin, y_wmin, x_vmax, y_vmax, x_vmin, y_vmin) { 
    let x_v, y_v; 
    let sx, sy; 
    sx = (x_vmax - x_vmin) / (x_wmax - x_wmin); 
    sy = (y_vmax - y_vmin) / (y_wmax - y_wmin); 

    x_v = (x_vmin + ((x_w - x_wmin) * sx)); 
    y_v = (y_vmin + ((y_w - y_wmin) * sy)); 

    return ({x:x_v, y:y_v});
} 

function Transfer(event) {
    let xWmax = canvas.width;
    let xWmin = 0;
    let yWmax = canvas.height;
    let yWmin = 0;

    console.log(rectPoints);

    let xVmax = rectPoints[1].x;
    let xVmin = rectPoints[0].x;
    let yVmax = rectPoints[1].y;
    let yVmin = rectPoints[0].y;
    scndCtx.beginPath();

    let xyW = bezier(0.01,points[0], points[1], points[2], points[3]);
    let xyV = windowtoViewport(xyW.x,xyW.y,xWmax, yWmax, xWmin, yWmin, xVmax, yVmax, xVmin, yVmin);
    scndCtx.moveTo(xyV.x, xyV.y);

    for (let i = 0.01; i < 1; i+= 0.01) {
        xyW = bezier(i,points[0], points[1], points[2], points[3]);
        xyV = windowtoViewport(xyW.x,xyW.y,xWmax, yWmax, xWmin, yWmin, xVmax, yVmax, xVmin, yVmin);
        scndCtx.lineTo(xyV.x, xyV.y);
    }
    scndCtx.rect(rectPoints[0].x, rectPoints[0].y, rectPoints[1].x - rectPoints[0].x, rectPoints[1].y - rectPoints[0].y);
    scndCtx.stroke();
}

function cleanCanvas(index) {
    if (index == 1) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        redraw();
    }
    else if (index == 2) {
        console.log("cleaning");
        scndCtx.clearRect(0, 0, secondCanvas.width, secondCanvas.height);
        redraw();
    }
    
}

function redraw() {

}

function drawRectangle(context, point1, point2, second = false) {
    let rectWidth = point2.x - point1.x;
    let rectHeight = point2.y - point1.y;
    if (!second) {
        context.drawImage(image, canvas.width / 2 - image.width / 2,
            canvas.height / 2 - image.height / 2);
    }
    context.strokeRect(point1.x, point1.y, rectWidth, rectHeight);
}


function cleanPoints() {
    points = [];
    secondPoints = [];
    gotcha = false;
    secondGotcha = false;
    cleanCanvas(1);
    foundPointIndex = -1;
    secondFoundPointIndex = -1;
}

function drawLinesBetweenPoints(color = 'red') {
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = color;
    for (let i = 0; i < points.length; i++) {
        if (points[i + 1]) {
            ctx.moveTo(points[i].x, points[i].y);
            ctx.lineTo(points[i + 1].x, points[i + 1].y);
        }
    }
    ctx.stroke();
}

function drawPoint(index = rectPoints.length - 1, color = 'black', fill = 'pink') {
    if (rectPoints[index]) {
        let pointToBeDrawn = rectPoints[index];
        scndCtx.beginPath();
        scndCtx.arc(pointToBeDrawn.x, pointToBeDrawn.y, pointRadius, 0, 2 * Math.PI, false);
        scndCtx.fillStyle = fill;
        scndCtx.fill();
        scndCtx.lineWidth = 2;
        scndCtx.strokeStyle = color;
        scndCtx.stroke();
        scndCtx.closePath();
    }
}

