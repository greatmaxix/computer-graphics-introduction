let canvas = document.getElementById("myCanvas");
canvas.mozImageSmoothingEnabled = false;
canvas.webkitImageSmoothingEnabled = false;
canvas.msImageSmoothingEnabled = false;
canvas.imageSmoothingEnabled = false;
canvas.mozImageSmoothingEnabled = false;
canvas.addEventListener('mousedown', mouseDownEventHandler);
canvas.addEventListener('mouseup', mouseUpEventHandler);

function radioBtsChange(event) {
    BSplineType = event.value;
    if (points.length > 3) {
        cleanCanvas();
        drawBSpline();
    }
}

let ctx = canvas.getContext("2d");
let points = [];
let pointRadius = 6;
let gotcha = false;
let foundPointIndex;
let BSplineType = 0;

function cleanCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function cleanPoints() {
    points = [];
    gotcha = false;
    cleanCanvas();
    foundPointIndex = -1;
}

function addPoint(clickCoords) {
    points.push(clickCoords);
}

function mouseDownEventHandler(event) {
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    let clickedCoords = { x: x, y: y };

    let foundPoint = points.findIndex((currPoint) =>
        ((clickedCoords.x >= currPoint.x - (pointRadius / 2) && clickedCoords.x <= currPoint.x + (pointRadius / 2))
            && (clickedCoords.y >= currPoint.y - (pointRadius / 2) && clickedCoords.y <= currPoint.y + (pointRadius / 2)))
    );
    if (foundPoint != -1) {
        foundPointIndex = foundPoint;
        gotcha = true;
    }
    else {
        addPoint(clickedCoords);
        drawPoint(points.length - 1);
    }
}

function mouseUpEventHandler(event) {
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    let clickCoords = { x: x, y: y };

    if (gotcha && foundPointIndex != -1) {        
        points[foundPointIndex] = clickCoords;
        foundPointIndex = -1;
        gotcha = false;
    }

    cleanCanvas();
    drawLinesBetweenPoints('black');
    
    points.forEach((point, index) => {
        drawPoint(index);
    })

    if (points.length > 3) {
        drawBSpline();
    }
}

function getKnotsAndHandleTypeChange(BSplineType, degree) {
    let numberOfPoints = points.length;
    let knots = [];
    //normal bslpine with clamped vector
    if (BSplineType == 0) {
        //first and last degree + 1 members are 0 and 1
        let max = numberOfPoints + degree + 1;
        for (let i = 0; i < max; i++) {
            if (i < degree + 1) {
                knots[i] = 0;
            }
            else if ((max - i) <= degree + 1) {
                knots[i] = degree;
            }
            else {
                knots[i] = i / (numberOfPoints - degree + 1);
            }
        }
    }
    //case of closed bslpine algorithm handles it
    else if (BSplineType == 1) {
        //algorithm will create unclamped knot vector {1,2,3,4,5..}
        //but we need to repeat first (degree + 1) points in the end
        for (let index = 0; index < degree + 1; index++) {
            points.push(points[index]);
        }
        knots = undefined;
    }
    else if (BSplineType == 2){
        //idk what is endpoint
        knots = undefined;
    }
    return knots;
}

function drawBSpline() {
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'red';
    let accuracy = 0.01;
    let degree = 2; //to be changable
    let weight = 2;
    //unclapmed!
    let knots = [];
    //creating knots
    knots = getKnotsAndHandleTypeChange(BSplineType, degree);
    console.log(knots);
    let bSpline = interpolate(0, degree, points, knots);
    ctx.moveTo(bSpline[0], bSpline[1]);
    for (let i = accuracy; i < 1; i+=accuracy) {
        bSpline = interpolate(i, degree, points, knots);
        ctx.lineTo(bSpline[0], bSpline[1]);
        //g.DrawLine(  new Pen(cl, width) , x - 1, y, x + 1, y);
        //g.DrawLine(new Pen(cl, width), x, y - 1, x, y + 1);
    }
    ctx.stroke();
    if (BSplineType == 1) {
        if (points[points.length -1] && points[points.length - 2] && points[points.length -3]) {
            points.splice(points.length - 3, 3);
        }
    }
}

function interpolate(t, degree, points, knots, weights) {
    var i,j,s,l;    
    var n = points.length;
    var d = 2;
  
    if(degree < 1) throw new Error('degree must be at least 1 (linear)');
    if(degree > (n-1)) throw new Error('degree must be less than or equal to point count - 1');
  
    if(!weights) {
      // build weight vector of length [n]
      weights = [];
      for(i=0; i<n; i++) {
        weights[i] = 1;
      }
    }
  
    if(!knots) {
      // build knot vector of length [n + degree + 1]
      var knots = [];
      for(i=0; i<n+degree+1; i++) {
        knots[i] = i;
      }
    } else {
      if(knots.length !== n+degree+1) throw new Error('bad knot vector length');
    }
  
    var domain = [
      degree,
      knots.length-1 - degree
    ];
  
    // remap t to the domain where the spline is defined
    var low  = knots[domain[0]];
    var high = knots[domain[1]];
    t = t * (high - low) + low;
  
    if(t < low || t > high) throw new Error('out of bounds');
  
    // find s (the spline segment) for the [t] value provided
    for(s=domain[0]; s<domain[1]; s++) {
      if(t >= knots[s] && t <= knots[s+1]) {
        break;
      }
    }
  
    // convert points to homogeneous coordinates
    var v = [];
    for(i=0; i<n; i++) {
        v[i] = [];
        for(j=0; j<d; j++) {
            if (j == 0) {
                v[i][j] = points[i].x * weights[i];
            }
            else if (j == 1) {
                v[i][j] = points[i].y * weights[i];    
            }
            //v[i][j] = points[i][j] * weights[i];
        }
        v[i][d] = weights[i];
    }
  
    var alpha;
    for(l=1; l<=degree+1; l++) {
      for(i=s; i>s-degree-1+l; i--) {
        alpha = (t - knots[i]) / (knots[i+degree+1-l] - knots[i]);
  
        for(j=0; j<d+1; j++) {
          v[i][j] = (1 - alpha) * v[i-1][j] + alpha * v[i][j];
        }
      }
    }
  
    // convert back to cartesian and return
    var result = result || [];
    for(i=0; i<d; i++) {
      result[i] = v[s][i] / v[s][d];
    }
  
    return result;
}

function drawLinesBetweenPoints(color = 'red') {
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = color;
    for (let i = 0; i < points.length; i++) {
        if (points[i + 1]) {
            ctx.moveTo(points[i].x, points[i].y);
            ctx.lineTo(points[i + 1].x, points[i + 1].y);
        }
    }
    ctx.stroke();
}

function drawPoint(index = points.length - 1, color = 'black', fill = 'pink') {
    if (points[index]) {
        let pointToBeDrawn = points[index];
        ctx.beginPath();
        ctx.arc(pointToBeDrawn.x, pointToBeDrawn.y, pointRadius, 0, 2 * Math.PI, false);
        ctx.fillStyle = fill;
        ctx.fill();
        ctx.lineWidth = 2;
        ctx.strokeStyle = color;
        ctx.stroke();
        ctx.closePath();
    }
}

